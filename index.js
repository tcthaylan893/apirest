const express = require('express')
const mongoose = require('mongoose')
const requireDir = require('require-dir')
const bodyParser = require('body-parser')

const cors = require('cors');

const app = express()

mongoose.connect(
    'mongodb://localhost:27017/api-imagens', 
    {useNewUrlParser: true}
)

requireDir('./src/models')

app.use(cors({credentials: true,origin: ['http://10.17.2.110:3001', "http://10.17.2.120:3000"]}))

app.use(bodyParser.json())

app.use(require('./src/routes'))

app.use('/public/images', express.static('./public/images'))

app.listen(3000, () => {
    console.log('Funcionou')
})