const multer = require('multer')

const uploadImage = (req, res) => {
    
    let storage = multer.diskStorage({
        destination: (req, file, cb) => {

            let path = __dirname.replace('src','')
           
            cb(null, path+'\\public\\images')
           
            },
        filename: (req, file, cb) => {
            
        cb(null, file.originalname)

    }
    })

    const upload = multer({storage: storage}).single('imagem')

    upload(req, res, (error) => {
        if (error) {
            console.log(error)
        } else {
            res.sendStatus(200)
        }
        // if (error) {
        //     res.error
        // } else {
        //     image.create({
        //         title: req.body.title,
        //         category: req.body.category,
        //         url: 'http://10.17.2.110:3000/public/images/' + req.file.originalname
        //     })
        //     res.sendStatus(200)
        // }
    })
}

module.exports = uploadImage