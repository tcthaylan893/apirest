const express = require('express')
const mongoose = require('mongoose')
const routes = express.Router()
const uploadImage = require('./uploadImage')
require('./models/Image')

const image = mongoose.model('Image')

routes.get('/', async (req, res) => {
    // res.setHeader('Access-Control-Allow-Origin' , 'http://localhost:3001')
    var teste = await image.find()
    res.send(teste)
})

routes.post('/addImage', (req, res) => {
    uploadImage(req, res, image)
})

routes.post('/addInfoImage', (req, res) => {
    image.create({
            title: req.body.title,
            category: req.body.category,
            url: req.body.url
    })
    res.sendStatus(200)
})

routes.put('/', async (req, res) => {

    const update = await image.updateOne({
        title: "Title 1"
    }, { 
        $set: {
            title: "..."
        }
    })
    res.send(update)
})

// routes.delete('/', async (req, res) => {
//     // const deletar = await image.deleteOne({
//     //     title: "Title 1"
//     // })

//     const deletar = await image.remove({})
//     console.log('............')
//     res.send(deletar)
// })

// routes.delete('/', async (req, res) => {
//     const dell = await image.remove({

//     })
//     res.send(dell)
// })

// app.get('/', (req, res) => {
//     console.log(req.query)
//     res.send('Hello World')
// })

module.exports = routes